var serverUrl = "http://app.receco.eu/";

function warningPluginIsObsolete() {
	$('#rececoHeader').append( "<a class='download' title='Existe una nueva versión del plugin' href='https://github.com/bromagosa/ReCeCoPlugin/raw/master/chrome/ReCeCoPlugin.crx'></a>" )
}

function openSideBar() {
	if ($('#rececoHeader')[0]) {
		$('#rececoSideBar').remove();
		$('#rececoHeader').remove();
	} else { 
		var sideBarHTML = 
			"<div id='rececoHeader'>"
			+ "<span>ReCeCo</span>"
			+ "<a class=\"close\" onClick='document.body.removeChild(document.getElementById(\"rececoSideBar\"));"
			+ "document.body.removeChild(document.getElementById(\"rececoHeader\"))'></a>"
			+ "<a class=\"home\" href='" + serverUrl + "backend' target='_blank'></a></div>"
			+ "<div class='receco' id='rececoSideBar'"
			+ "'><iframe src='" + serverUrl + "frontend?url={:"
			+ encodeURIComponent(location.href.replace("#","_ReCeCoHash_")) 
			+ "}&title={:" 
			+ encodeURIComponent(document.title.replace("#","_ReCeCoHash_"))
			+ "}'></iframe></div>";
		$('body').append(sideBarHTML);
	}
};
