serverUrl = 'http://app.receco.eu/';

function isPluginObsolete(tab) {
	$.getJSON( 
		chrome.extension.getURL("manifest.json") ,
		function(localManifest) { 
			$.getJSON(
				'https://raw.github.com/bromagosa/ReCeCoPlugin/master/chrome/dev/manifest.json' ,
				function(remoteManifest) {
					if( remoteManifest.version > localManifest.version ) { chrome.tabs.executeScript(tab.id, { code: 'warningPluginIsObsolete()' } ) }
				}
			)
		}
	)
}

function updateIcon(isTagged , tabId) {
  chrome.pageAction.show(tabId);
  if (isTagged) {
	chrome.pageAction.setIcon({tabId: tabId , path: 'icons/greenIcon.png'})
	chrome.pageAction.setTitle({tabId: tabId , title: 'Documento etiquetado en ReCeCo'})
  }
};

function showWindow(tab) {
	chrome.tabs.executeScript(tab.id, {code: 'openSideBar()'});
	isPluginObsolete(tab);
}

function peekUrl(tabId, changeInfo, tab) {
	$.ajax({
	  type: 'GET',
	  url: serverUrl,
	  data: {peekUrl: encodeURIComponent(tab.url)},
	  dataType: 'json',
	  timeout: 500,
	  success: function(response) { updateIcon(response , tabId); }
	})
};

chrome.tabs.onUpdated.addListener(peekUrl);
chrome.pageAction.onClicked.addListener(showWindow);
